<%@ page import="com.ub.core.base.routes.BaseRoutes" %>
<%@ page import="com.ub.core.language.routes.LanguageAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--@elvariable id="language" type="String"--%>

<header id="header" class="page-topbar">
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                <ul class="left">
                    <li>
                        <h1 class="logo-wrapper">
                            <a href="<%=BaseRoutes.ADMIN%>" class="brand-logo darken-1">
                                <img src="<c:url value="/static/ub/images/logo-horizont.png"/>" alt="UnitBean logo">
                            </a>
                            <span class="logo-text">UnitBean</span>
                        </h1>
                    </li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a class="waves-effect waves-block waves-light translation-button"
                           data-activates="translation-dropdown">
                            <c:if test="${language eq 'en'}">
                                <img src="<c:url value="/static/ub/images/flag-icons/United-States.png"/>"
                                     alt="English"/>
                            </c:if>
                            <c:if test="${language eq 'ru'}">
                                <img src="<c:url value="/static/ub/images/flag-icons/Russia.png"/>" alt="Russian"/>
                            </c:if>
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-block waves-light toggle-fullscreen">
                            <i class="mdi-action-settings-overscan"></i>
                        </a>
                    </li>
                </ul>
                <ul id="translation-dropdown" class="dropdown-content">
                    <li>
                        <c:url value="<%= LanguageAdminRoutes.CHANGE%>" var="changeLangUrl">
                            <c:param name="lang" value="en"/>
                        </c:url>
                        <a href="<c:url value="${changeLangUrl}"/>">
                            <img src="<c:url value="/static/ub/images/flag-icons/United-States.png"/>" alt="English"/>
                            <span class="language-select">English</span>
                        </a>
                    </li>
                    <li>
                        <c:url value="<%= LanguageAdminRoutes.CHANGE%>" var="changeLangUrl">
                            <c:param name="lang" value="ru"/>
                        </c:url>
                        <a href="<c:url value="${changeLangUrl}"/>">
                            <img src="<c:url value="/static/ub/images/flag-icons/Russia.png"/>" alt="Russian"/>
                            <span class="language-select">Russian</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>