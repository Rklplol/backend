<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <span>Copyright © 2013 - 2017 <strong>UBCore</strong> Admin panel.</span>
            <span class="right"><s:message code="ubcore.admin.developed" text="Developed by"/>
                <a class="grey-text text-lighten-4" href="http://www.unitbean.com" target="_blank">LLC UnitBean</a>
            </span>
        </div>
    </div>
</footer>