<section>
    <div class="container article-title">
        <div class="row">
            <h1 class="article-title-h1">
                TVD-property for linear differential schemes of higher than first order approximation
            </h1>
        </div>
    </div>
</section>

<section>
    <div class="container article-pic">
        <div class="row">
            <div class="col-md8">
                <img class="article-pic-img" src="/static/backend/img/Snowstorm.jpg"/>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article-audio">
        <div class="row">
            <div class="col-md-4 offset-md-4">
            <audio src="/static/backend/audios/Packster%20-%20Snowstorm.mp3" controls preload="metadata"></audio>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 article-content">
                Monotone schemes are attractive for solving engineering and scientific problems because they do not produce non-physical solutions.

                Godunov's theorem proves that linear schemes which preserve monotonicity are, at most, only first order accurate. Higher order linear schemes, although more accurate for smooth solutions, are not TVD and tend to introduce spurious oscillations (wiggles) where discontinuities or shocks arise.

                To overcome these drawbacks, various high-resolution, non-linear techniques have been developed, often using flux/slope limiters.

                From Wikipedia.
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article-comment-add">
        <div class="row">
            <div class="col-sm-1">
                <div class="article-comment-add-userpic"></div>
            </div>
            <div class="col-sm-10">
                <div class="article-comment-add-comment">
                    <input class="article-comment-add-comment-input" value="Ваш комментарий...">
                </div>
            </div>
            <div class="col-sm-1">
                <div class="article-comment-add-submit">
                    <button class="article-comment-add-submit-btn"></button>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article-comment-list">
        <div class="row">

            <div class="col-md-12 article-comment-list-item">
                <div class="article-comment-list-userpic">
                    <img src="/static/backend/img/C.jpg">
                </div>
                <div class="article-comment-list-content">
                    <a href="/">Rklplol</a>
                    <div class="article-comment-list-content-date">
                    20 янв, 2018
                    </div>
                    <div class="article-comment-list-content-text">
                    Sample text here...
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article-comment-list">
        <div class="row">

            <div class="col-md-12 article-comment-list-item">
                <div class="article-comment-list-userpic">
                    <img src="/static/backend/img/B.jpg">
                </div>
                <div class="article-comment-list-content">
                    <a href="/">Packster</a>
                    <div class="article-comment-list-content-date">
                        21 янв, 2018
                    </div>
                    <div class="article-comment-list-content-text">
                        Позорище!
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>