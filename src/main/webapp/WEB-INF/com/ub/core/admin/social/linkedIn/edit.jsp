<%@ page import="com.ub.core.social.linkedIn.routes.LinkedInAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form:form action="<%= LinkedInAdminRoutes.SETTINGS_EDIT %>" class="card-panel" modelAttribute="doc" method="post"
               autocomplete="off">
        <h4 class="header2">Изменение настроек Linked In</h4>

        <jsp:include page="/WEB-INF/com/ub/core/admin/components/errorAlert.jsp"/>

        <form:hidden path="id"/>

        <div class="row">
            <div class="input-field col s12">
                <label for="client_id">Client id</label>
                <form:input path="client_id" id="client_id"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="client_secret">Client secret</label>
                <form:input path="client_secret" id="client_secret"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="redirect_uri">Redirect uri</label>
                <form:input path="redirect_uri" id="redirect_uri"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="response_type">Response type</label>
                <form:input path="response_type" id="response_type"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="state">State</label>
                <form:input path="state" id="state"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="scope">Scope</label>
                <form:input path="scope" id="scope"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>
