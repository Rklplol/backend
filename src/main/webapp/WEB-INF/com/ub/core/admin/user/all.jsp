<%@ page import="com.ub.core.user.model.UserStatus" %>
<%@ page import="com.ub.core.user.routes.UserAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--@elvariable id="search" type="com.ub.core.user.view.search.SearchUserResponse"--%>

<c:set var="STATUS_ACTIVE" value="<%=UserStatus.ACTIVE%>"/>
<c:set var="STATUS_BLOCK" value="<%=UserStatus.BLOCK%>"/>

<div class="section">
    <form:form class="row" action="<%=UserAdminRoutes.ALL%>" modelAttribute="search" method="get">
        <div class="input-field col s12 m4 l3">
            <form:select path="userStatus">
                <form:option value="">Все</form:option>
                <form:options items="<%= UserStatus.values() %>" itemLabel="title"/>
            </form:select>
        </div>
        <div class="input-field col s9 m6 l8">
            <form:input id="search" type="text" path="query" placeholder="Search"/>
        </div>
        <div class="input-field col s3 m2 l1">
            <div class="input-field">
                <form:button class="btn-floating waves-effect waves-light right" type="submit">
                    <i class="mdi-action-search"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>

<div class="section">
    <table class="responsive-table bordered">
        <thead>
        <tr>
            <th>Email</th>
            <%--<th>Login</th>
            <th>Телефон</th>--%>
            <th>Имя</th>
            <th>Статус</th>
            <th>Роли</th>
            <%--<th><s:message code="ubcore.admin.createdAt"/></th>--%>
            <th><s:message code="ubcore.admin.updateAt"/></th>

            <th><s:message code="ubcore.admin.action"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${search.result}" var="doc" varStatus="status">
            <tr>
                <td class="u-text-break-all">${doc.noNullEmail}</td>
                <%--<td class="u-text-break-all">${doc.login}</td>
                <td class="u-text-break-all">${doc.phoneNumber}</td>--%>
                <td class="u-text-break-all">${doc.name}</td>
                <td>${doc.status.title}</td>
                <td>
                    <c:forEach items="${doc.roles}" var="role">
                        ${role.title};
                    </c:forEach>
                </td>
                <%--<td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                    value="${doc.createdAt}"/></td>--%>
                <td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                    value="${doc.updateAt}"/></td>

                <td>
                    <c:url value="<%=UserAdminRoutes.REMOVE%>" var="removeUrl">
                        <c:param name="id" value="${doc.id}"/>
                    </c:url>
                    <a href="${removeUrl}" class="btn btn-small waves-effect waves-light red js-link-remove-doc">
                        <s:message code="ubcore.admin.action.remove"/><i class="mdi-action-delete left"></i>
                    </a>
                    <c:url value="<%=UserAdminRoutes.EDIT%>" var="editUrl">
                        <c:param name="id" value="${doc.id}"/>
                    </c:url>
                    <a href="${editUrl}" class="btn btn-small waves-effect waves-light blue">
                        <s:message code="ubcore.admin.action.edit"/><i class="mdi-editor-mode-edit left"></i>
                    </a>
                    <c:if test="${doc.status eq STATUS_ACTIVE}">
                        <c:url value="<%=UserAdminRoutes.BLOCK%>" var="blockUrl">
                            <c:param name="id" value="${doc.id}"/>
                        </c:url>
                        <a href="${blockUrl}" class="btn btn-small waves-effect waves-light red js-link-warning-action">
                            Block<i class="mdi-content-block left"></i>
                        </a>
                    </c:if>
                    <c:if test="${doc.status eq STATUS_BLOCK}">
                        <c:url value="<%=UserAdminRoutes.ACTIVE%>" var="activeUrl">
                            <c:param name="id" value="${doc.id}"/>
                        </c:url>
                        <a href="${activeUrl}"
                           class="btn btn-small waves-effect waves-light red js-link-warning-action">
                            Active<i class="mdi-action-verified-user left"></i>
                        </a>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="section">
    <div class="row center-align">
        <ul class="pagination">
            <c:url value="<%=UserAdminRoutes.ALL%>" var="prevUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.prevNum}"/>
            </c:url>
            <li class="${search.prevNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.prevNum ne search.currentPage}">href="${prevUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-left"></i>
                </a>
            </li>

            <c:forEach items="${search.pagination}" var="page">
                <c:url value="<%=UserAdminRoutes.ALL%>" var="pageUrl">
                    <c:param name="query" value="${search.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="${search.currentPage eq page ? 'active' : ''}">
                    <a href="${search.currentPage ne page ? pageUrl : ''}">${page + 1}</a>
                </li>
            </c:forEach>

            <c:url value="<%=UserAdminRoutes.ALL%>" var="nextUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.nextNum}"/>
            </c:url>
            <li class="${search.nextNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.nextNum ne search.currentPage}">href="${nextUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>