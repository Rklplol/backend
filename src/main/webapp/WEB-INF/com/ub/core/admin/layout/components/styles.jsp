<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<link rel="icon" href="<c:url value="/static/ub/images/favicon/32.png"/>" sizes="32x32">
<link rel="icon" href="<c:url value="/static/ub/images/favicon/152.png"/>" sizes="144х144">
<link rel="icon" href="<c:url value="/static/ub/images/favicon/152.png"/>" sizes="152х152">
<link rel="apple-touch-icon-precomposed" href="<c:url value="/static/ub/images/favicon/152.png"/>">
<meta name="msapplication-TileImage" content="<c:url value="/static/ub/images/favicon/144.png"/>">
<meta name="msapplication-TileColor" content="#00bcd4">
<meta name="theme-color" content="#00bcd4">

<link href="<c:url value="/static/ub/css/main.css"/>" type="text/css" rel="stylesheet" media="screen,projection">
<link href="<c:url value="/static/ub/js/plugins/prism/prism.css"/>" type="text/css" rel="stylesheet" media="screen,projection">
<link href="<c:url value="/static/ub/js/plugins/perfect-scrollbar/perfect-scrollbar.css"/>" type="text/css" rel="stylesheet" media="screen,projection">
<link href="<c:url value="/static/ub/js/plugins/sweetalert/dist/sweetalert.css"/>" type="text/css" rel="stylesheet" media="screen,projection">
<link href="<c:url value="/static/ub/js/plugins/dropify/css/dropify.min.css"/>" type="text/css" rel="stylesheet" media="screen,projection">

<tiles:importAttribute name="cssLinks"/>
<c:forEach items="${cssLinks}" var="cssLink">
    <link href="<c:url value="${cssLink}"/>" type="text/css" rel="stylesheet" media="screen,projection">
</c:forEach>

<tiles:insertAttribute name="head"/>