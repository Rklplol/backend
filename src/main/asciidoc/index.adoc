= RESTful API Guide
API;
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 4
:sectlinks:

[[overview]]
= Обзор

[[overview-http-verbs]]
== HTTP методы

Мы будем придерживаться как можно ближе к стандартным HTTP методам.

|===
| Метод | Использование

| `GET`
| Чтение ресурса

| `POST`
| Взаимодействие с ресурсом

| `DELETE`
| Удаление ресурса

| `PATCH`
| Обновление ресурса
|===

[[overview-http-status-codes]]
== HTTP статусы

Так же как и с HTTP методами, мы будет придерживаться стандартным HTTP статусам. Но будет использовать лишь основные.

|===
| Статус | Использование

| `200 OK`
| Успешный запрос

| `400 Bad Request`
| Некорректный запрос, ошибки в параметрах

| `401 User Not Authorized`
| Запрос не авторизирован или недостаточно прав для просмотра ресурса

| `404 Not Found`
| Запрашиваемый ресурс не найден
|===

[[overview-auth]]
== Авторизация

Авторизация запросов происходит по средству помещения параметров в header запроса.

|===
| Header | Значение

| `X-Auth-Token`
| Токен пользователя

| `X-Device-Token`
| UUID устройства (необязательно)

| `X-Device-Type`
| Тип устройства [ANDROID, IOS] (необязательно)

| `Accept-Language`
| Язык ответа [ru | en] (необязательно)
|===

[[overview-respinse-errors-format]]
== Формат ошибок

Формат ошибок возвращаемых серверов, надо учитывать, что такие ошибки возвращаются сервером вместе с HTTP статусами: 400, 401, 404.

|===
| Path | Type | Description

| message
| String
| Сообщение ошибки (может не быть)

| errors
| Object
| Ошибки

| errors.field
| Array
| Ошибки поля 'field'

| errors.field[]
| String
| Текст ошибки
|===

[[overview-respinse-result-format]]
== Формат результат

Формат результата успешного запроса, надо учитывать, что такой ответ возвращается сервером вместе HTTP статусом 200.

|===
| Path | Description

| result
| Результат запроса
|===

[[base]]
= Базовые API запросы

[[resources-base-example]]
== Загрузка изображения

`POST` запрос.

=== Пример запроса

include::{snippets}/base/example/upload/curl-request.adoc[]

=== Параметры запроса

include::{snippets}/base/example/upload/request-parameters.adoc[]

=== Пример ответа

include::{snippets}/base/example/upload/http-response.adoc[]

=== Поля ответа

include::{snippets}/base/example/upload/response-fields.adoc[]

