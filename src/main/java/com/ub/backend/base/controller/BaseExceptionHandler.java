package com.ub.backend.base.controller;

import com.ub.core.base.api.response.ErrorApiResponse;
import com.ub.core.base.exception.BadRequestApiException;
import com.ub.core.base.exception.ResourceNotFoundException;
import com.ub.core.security.exception.UserBlockedException;
import com.ub.core.user.exception.UserNotExistException;
import com.ub.core.user.exception.UserTokenExpiredException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice()
public class BaseExceptionHandler {
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    public ErrorApiResponse userExist(ResourceNotFoundException e) {

        return new ErrorApiResponse("resource not found");
    }

    @ResponseBody
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UserTokenExpiredException.class)
    public ErrorApiResponse UserTokenExpiredException(UserTokenExpiredException e) {
        BadRequestApiException ex = new BadRequestApiException(e.getMessage(),"token","error.token.expired");
        return new ErrorApiResponse(ex);
    }

    @ResponseBody
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UserBlockedException.class)
    public ErrorApiResponse UserBlockedException(UserBlockedException e) {
        BadRequestApiException ex = new BadRequestApiException(e.getMessage(),"token","error.token.expired");
        return new ErrorApiResponse(ex);
    }

    @ResponseBody
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UserNotExistException.class)
    public ErrorApiResponse UserNotExistException(UserNotExistException e) {
        BadRequestApiException ex = new BadRequestApiException(e.getMessage(),"token","error.token.expired");
        return new ErrorApiResponse(ex);
    }
}

