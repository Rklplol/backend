package com.ub.backend.base.utils;

import com.ub.core.base.utils.RequestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class RequestLangUtils extends RequestUtils {
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String DEFAULT_ACCEPT_LANGUAGE = "ru";

    public static String getLanguage() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest()
                .getHeader(ACCEPT_LANGUAGE);
    }

    public static String getLanguageWithDefault(){
        try{
            String acceptLanguage = getLanguage();
            return acceptLanguage != null ? acceptLanguage : DEFAULT_ACCEPT_LANGUAGE;
        }catch (IllegalStateException ex){
            return DEFAULT_ACCEPT_LANGUAGE;
        }
    }
}
