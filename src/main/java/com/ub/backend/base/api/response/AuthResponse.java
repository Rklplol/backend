package com.ub.backend.base.api.response;

import com.ub.core.user.model.UserTokenModel;

public class AuthResponse {
    private UserTokenModel accessToken;
    private UserTokenModel refreshToken;

    public AuthResponse(){
        this.accessToken = new UserTokenModel();
        this.refreshToken = new UserTokenModel();
    }

    public UserTokenModel getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(UserTokenModel refreshToken) {
        this.refreshToken = refreshToken;
    }

    public UserTokenModel getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(UserTokenModel accessToken) {
        this.accessToken = accessToken;
    }
}
