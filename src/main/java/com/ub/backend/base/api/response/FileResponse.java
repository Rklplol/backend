package com.ub.backend.base.api.response;

public class FileResponse {
    private String newFileId;

    public FileResponse(String s) {
        this.newFileId = s;
    }

    public FileResponse() {}

    public String getNewFileId() {
        return newFileId;
    }

    public void setNewFileId(String newFileId) {
        this.newFileId = newFileId;
    }
}
