package com.ub.backend.article.controller;

import com.ub.backend.article.routes.ArticleClientRoute;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ArticleClientController {
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String all(){
        return "com.ub.backend.client.article.all";
    }
    @RequestMapping(value=ArticleClientRoute.ADD, method=RequestMethod.GET)
    public String add(){
        return "com.ub.backend.client.article.add";
    }
    @RequestMapping(value=ArticleClientRoute.SUCCESS, method=RequestMethod.GET)
    public String success(){ return "com.ub.backend.client.article.success"; }
    @RequestMapping(value=ArticleClientRoute.VIEW, method=RequestMethod.GET)
    public String view(){
        return "com.ub.backend.client.article.view";
    }
}
