package com.ub.backend.article.menu;

import com.ub.backend.article.routes.ArticleAdminRoutes;
import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;

public class ArticleAllMenu extends BaseMenu {
    public ArticleAllMenu(){
        this.name="Все";
        this.icon=MenuIcons.MDI_ACTION_DESCRIPTION;
        this.parent=new ArticleMenu();
        this.url=ArticleAdminRoutes.ALL;
    }
}
