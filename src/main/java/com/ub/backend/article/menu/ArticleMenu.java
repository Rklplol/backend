package com.ub.backend.article.menu;

import com.ub.backend.article.routes.ArticleAdminRoutes;
import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;

public class ArticleMenu extends BaseMenu {
public ArticleMenu(){
        this.name="Статьи";
        this.icon=MenuIcons.MDI_ACTION_DESCRIPTION;
        }
}